{ pkgs ? import <nixpkgs> { } }:

pkgs.mkShell {
  buildInputs = with pkgs; [

  aalib
  alsaUtils
  coreutils
  lsd
  lsof
  shellcheck
  sl

  ];
}
