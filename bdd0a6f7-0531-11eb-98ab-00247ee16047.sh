function ls_command () {
    for flag in -t -a -r -t -a -R -rR -rt -Rt -Rrt
    do
        $1 $flag "$HOME" | aplay
    done

}

if command -v lsd; then
    ls_command lsd 
elif command -v ls; then 
    ls_command ls
else 
    printf "This piece requires lsd or ls installed\n" 1>&2
    exit 1
fi 


