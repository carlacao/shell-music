![](aaron-burden-WFzA16YoHcI-unsplash.jpg)

Shell Music is a collection of pieces to be played by a computer shell language like bash. 

The pieces have only been tested on machines running GNU/Linux. 

Individual pieces can be played by running bash on the title of the piece in your shell. 

For example: 

```sh
bash cf7d75e0-0531-11eb-90fb-00247ee16047.sh
```

# Dependencies

aalib

sl

lsd

alsa-utils

coreutils

# Nix 

If you are using the Nix package manager then you can run the following command in the root of the directory to install all the dependencies:

```sh
cd shell-music
nix-shell
```

# Contributing

Feel free to submit a pull request for a new piece or to fix and/or extend a current one.
